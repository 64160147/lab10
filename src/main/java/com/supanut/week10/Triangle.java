package com.supanut.week10;

public class Triangle extends Shape{
    private double a ;
    private double b ;
    private double c ;
    public Triangle(double a ,double b , double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public String toString() {
        return this.getName() + " a: "+this.a + "/ b: " + this.b + "/ c: " + this.c;
    }

    @Override
    public double calArea() {
        double area;
        double s = (a + b + c)/2;
        area = (double)(Math.sqrt(s*(s-a)*(s-b)*(s-c)));
        return area;
    }

    @Override
    public double calPerimeter() {
        return this.a + this.b + this.c;
    }
}
