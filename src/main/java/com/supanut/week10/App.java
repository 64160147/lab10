package com.supanut.week10;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println();
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.printf("1. %s area: %.2f \n",rec1.getName(),rec1.calArea());
        System.out.printf("2. %s perimeter: %.2f \n",rec1.getName(),rec1.calPerimeter());
        System.out.println();

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("1. %s area: %.2f \n",rec2.getName(),rec2.calArea());
        System.out.printf("2. %s perimeter: %.2f \n",rec2.getName(),rec2.calPerimeter());
        System.out.println();

        Circle circle1 = new Circle(2);
        System.out.println(circle1.toString());
        System.out.printf("1. %s area: %.3f \n",circle1.getName(),circle1.calArea());
        System.out.printf("2. %s perimeter: %.3f \n",circle1.getName(),circle1.calPerimeter());
        System.out.println();

        Circle circle2 = new Circle(3);
        System.out.println(circle2.toString());
        System.out.printf("1. %s area: %.2f \n",circle2.getName(),circle2.calArea());
        System.out.printf("2. %s perimeter: %.2f \n",circle2.getName(),circle2.calPerimeter());
        System.out.println();

        Triangle Tri = new Triangle(2,2,2);
        System.out.println(Tri.toString());
        System.out.printf("1. %s area: %.2f \n",Tri.getName(),Tri.calArea());
        System.out.printf("2. %s perimeter: %.2f \n",Tri.getName(),Tri.calPerimeter());
        System.out.println();
    }
}
